[TOC]


This Bitbucket project repository consists of this, single, README.md file. 

It contains my philosophy on how to create an issue on a typical Bitbucket project. I made it public so I can link to this page from the 'Create Issue' pages of my other Bitbucket projects. I would welcome any feedback / opinions on how to improve it.

# Tips for creating an issue on [Bitbucket's Issue Tracker](https://confluence.atlassian.com/display/BITBUCKET/Use+the+issue+tracker) #

Please read this before filling in the form to create a new issue.

## Types of Issues
We can use this tool, (Bitbucket's Issue Tracker) to list various 'issues' that need to be dealt with:

* a '**[user story](#markdown-header-creating-a-new-user-story)**' that needs to be satisfied. A user story is a way to catalogue a *benefit* that our project offers to its *stakeholders*. See  [A 10 Minute Video Introducing User Stories](https://www.youtube.com/watch?v=NzSsE37opB0&index=3&list=PLUBvwdhqPYFmoszHUb4gy9UtVDvRwgpQc) 
* a '**[task](#markdown-header-creating-a-new-task)**' - something that needs to be done in order to ensure the **most important** user stories are satisfied by our project.
* a '**[bug](#markdown-header-creating-a-new-bug)**' -  a problem that needs to be discussed and resolved when there is a mismatch between *what you expect* and *what actually happens*. See [How to Write A Quality Bug Report](http://university.utest.com/writing-quality-bug-reports-and-utest-etiquette/)
* a '**[foreboding](#markdown-header-creating-a-new-foreboding)**' - an acknowledgement of something that can undermine the success of the project. By logging this 'risk' in the system we can face up to it, keep track of the danger and take action to mitigate it's effects.

* a '**[question](#markdown-header-creating-a-new-question)**' - a question about any aspect of the project.

Anyone can add an issue. But, please understand Bitbucket's Issue tool is not 100% perfectly designed for the way we want to use it. It was not designed to store 'user stories', 'forebodings' or 'questions'. So, we have to hack it a bit.

If you do one thing...**please ensure that the *title* is formatted correctly** according to the *type* of the issue.

If you are trying to add an issue that is not a User Story, Task, Bug or Foreboding - see me ( @johnniewalkeruk ) to discuss.  

##Titles and Descriptions 

The *title* of your new issue should be formatted in various ways to help us pick the one's of interest from the list.

Likewise, the *description* should, ideally, be formatted in a specific ways to convey the key information needed for people to resolve the issue. See below for details.

##Assignee
This describes *who* is going to resolve this issue. If you are unsure, either leave it unset or assign the issue to yourself.

##Priority

Bitbucket's Issue Tracker has a bit too many options here. (Ideally we would only have 3; high, medium and low).

We hope to employ a "fix bugs first" issue management strategy. So, to ensure that all bugs are dealt with *before* feature requests, we should insert bugs at either Critical or Blocker levels and never insert other types of Issue at those levels.

##Milestone

Some projects don't have Milestones. But for those that do, when creating an issue, we can attach a 'milestone' to it. This is so that they can be deferred or dealt-with appropriately.

See the project's wiki for a full list of milestones.

## Creating a new *User Story* 

#### Title for a User Story:

A user story is like an item in the wishlist of things that we want our project to achieve. But, we don't express it in an idealistic way like 'wouldn't it be great if our users could do xyz on our website'. Instead, we write it so it shows how a *feature* is linked to the *people involved in the project* and *their goals*. 

This rigour in formatting helps us 
* stay grounded and stops us running off with every idea under the sun
* helps us to compare potential features and separate the essentials from the nice-to-haves.

> User Story: As a [role] I/we [can/cannot] [do/know/have something/outcome] [to acheive my goal or objective]

Example User Story Title:

> User Story: *As a* project team member *I* can edit the project documentation myself so I can contribute to it without creating extra work for other team members.

####Description for a User Story 

Most of it is already in the title. I tend to just leave the desciption empty if the title is suffice. It should be pretty self-explanitory. People can use the comments system to ask for any clarifications once it has been submitted.


####Other Settings for a User Story 

* *Kind* should always be set to *enhancement*.
* *Priority* - always *trivial*. Its priority will increase if it is deemed useful. Never *critical* or *blocker*, those are reserved for bugs and the tasks involved in fixing them (see above).
* *Milestone* - If present, choose from our list of milestones.

## Creating a new *Task* 

#### Title for a Task: 

Should have the prefix 'Task' and then a description of the goal or end-state that we are aiming for.

> Task: [goal/end-state]

Example Task Title:

> Task: Vision statement created, reviewed and baselined

If you want to associate the task to a particular User Story:

> Task for User Story #??: Vision statement created, reviewed and baselined

or become a subtask (note we still have the `Task` prefix to ensure it is listed amongst other tasks):

> Task > Subtask for Task #??: Replace Drupal's default Home Page content with Basic Page

####Description for a Task 

This should be written in a way that tells us what the *goal* is, *why* we are doing this and *how* you expect it to be done. So, use Bitbucket's 'markdown' feature to format it like so:

------------

```
#!markdown

## Goal 
[goal]
## Why 
[why we are bothering]
## How 

[How it should be done

* Step 1
* Step 2
* etc.
]

-------

Extra notes.

```

------------

Example Task Description: 

------------

```
#!markdown

## Goal 

Ensure a 'vision statement' is created, reviewed and baselined.

## Why 

Sharing a vision is useful to help keep the team focused...
...we should 'baseline' it to ensure it does not change too frequently
during certain phases of the project. i.e It is tricky to hit a moving target.

## How 
We should chat ... to hone it and then have a meeting 
where fix it in place to officially baseline it.

------

May I suggest something like:

> We are going to create a financially viable web site that can ...
```

------------

####Other Settings for a Task

* *Kind* should always be set to *task*.
* *Priority* - Always *trivial*. Never *critical* or *blocker* (see above).
* *Milestone* - If present, choose from our list of milestones.

## Creating a new *Bug* 

#### Title for a Bug:

By nature, every bug report is describing something that is not working as it should. Be specific about what makes it "not working."

> Bug: [how reality differs from expectation]

Example Bug Title:

> Bug: Video doesn't play sound in Internet Explorer version 9

####Description for a Bug 

This should be written in a way that tells us 
* the actions performed that caused the bug so other team members can also recreate it and therefore see what needs fixing.
* what actually happened.
* what we expected


Use Bitbucket's 'markdown' feature to format it like so:

------------

```
#!markdown

## Actions Performed
[If applicable, describe which kind of operating system, device, browser, etc you were using.]
[Describe steps investigators should take to recreate the bug.]

##Actual Result
[What actually happened.]

## Expected 
[What you expected to happen.]

-------

[Extra notes/attachments].
```

------------

Example Bug Description : 

------------

```
#!markdown

## Actions Performed 
* Use Internet Explorer 9 on Apple Macbook Leopard X 
* Visit Youtube and watch a random video to ensure sound is playing.
* Visit our *Watch Video* page.
* Start playing the video


##Actual Result
I saw the video but did not hear any sound.

## Expected 
I expect to see and hear the video content.

-------

```

------------

####Other Settings for a Bug 

* *Kind* should always be set to *bug*.
* *Priority* - Either *critical* or *blocker*. This makes all bugs initially appear at the top of the list. If they are deemed less important we can reduce them later.
* *Milestone* - If present, choose from our list of milestones.



## Creating a new *Foreboding* 

There are many things that can cause a project to fail. We could be optimists and ignore these 'risks' and carry on regardless. We could be pessimists and give up on the project when we come to the first hurdle. Or, we can take the middle ground and acknowledge problems, gauge their potential danger, gauge their likehood of occuring, work out how to spot when they are cropping up and think of ways to tackle them.

We cannot spend ages worrying about risks. So, by quickly logging and ranking these worries as 'forebodings' we will be able to identify the 'Top 10 Risks' to care about, take action and spend a couple of hours reviewing the situation after each phase of the project.

#### Title for a Foreboding: 

Should have:

*  the prefix 'Foreboding:', then
* the Exposure Level (i.e. a number representing *likelihood multiplied by impact*, see below), and then 
* a description of the scenario that you fear.

> Foreboding:  (Level [Exposure Level]) - [summary of fearful scenario]

Example Foreboding Title:

> Foreboding: (Level 3) - The Bitbucket service disappears off the face of the earth and we lose our project info.

####Description for a Foreboding 

This should:
 
* elaborate on the risk being identified and explain how it might affect the project.
* estimate the likelihood (chance of the risk happening) on a score of 1 to 3. (3 = highly likely).
* estimate the impact  (chance it will stop the project succeeding) on a score of 1 to 3. (3 = highly damaging).
* identify signs that the risk is looming.
* identify steps we can take to fend off or mitigate the risk.

Here is the suggested format:

-----------

```
#!markdown

## Risk 

[Description]

## Exposure Level 
* Likelihood Score: [?] out of 3 
* Impact Score: [?] out of 3
* Exposure Score: (Likelihood x Impact) = [?]

## Indicators 

* [...indicator...]
* [...indicator...]
* [...indicator...]

## Mitigating Actions 

* [...action...]
* [...action...]
* [...action...]

```

------

Example Foreboding Description : 

-----

```
#!markdown

## Risk 

The Bitbucket service disappears off the face of the earth
and we lose our project info.

## Exposure Level 
* Likelihood Score: 1 out of 3 (Low)
* Impact Score: 3 out of 3 (High)
* Exposure Score: (Likelihood x Impact) = 1 x 3 = 3

## Indicators 

* Bitbucket stops working as expected.
* Bitbucket gets very slow to use.
* Bitbucket news announces closure.
* Bitbucket owners (Atlassian) get into financial trouble.

## Mitigating Actions 

* Make backups of the data, as often as possible, 
    to limit the potential disruption.
* Subscribe to Atlassian and Bitbucket news feeds.
* Set up Google Alerts to get alerted about Atlassian and Bitbucket.

```
--------

####Other Settings for a Foreboding

* *Kind* should always be set to *proposal*.
* *Priority* - Always *trivial*. Never *critical* or *blocker* (see above).
* *Milestone* - If present, the most appropriate time when we should worry about this risk.


## Creating a new *Question* 

#### Title for a Question:

Simply write the prefix `Question:` followed by a succinct summary of your question.

> Question: [Summary]

Example Question:

> Question: What does MRHA stand for?

####Description for a Question 

Most of it is already in the title. I tend to just leave the description empty if the title is suffice. If not, use the Description section to flesh out more detail.


####Other Settings for a Question 

* *Kind* should always be set to *proposal*.
* *Priority* - always *trivial*. Its priority will increase if it is deemed useful. Never *critical* or *blocker*, those are reserved for bugs and the tasks involved in fixing them (see above).
* *Milestone* - If present, choose from our list of milestones.